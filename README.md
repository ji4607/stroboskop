# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://ji4607@bitbucket.org/ji4607/stroboskop.git
git add jscolor.js
git commit -a -m "Priprava potrebnih JavaScript knjižnic"
git push origin master
```

Naloga 6.2.3:
https://bitbucket.org/ji4607/stroboskop/commits/ea5eca24e15e3f22699328af4a5cb1991ff36c3d


## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ji4607/stroboskop/commits/ae8b4353b4389cdd052c4da512e66148a3a23b0e

Naloga 6.3.2:
https://bitbucket.org/ji4607/stroboskop/commits/2070ff8e5d14a34f5990c4b715bbca55aa4cf707

Naloga 6.3.3:
https://bitbucket.org/ji4607/stroboskop/commits/2061c23b939c99be5a66f0434e1e4b0aef333ad3

Naloga 6.3.4:
https://bitbucket.org/ji4607/stroboskop/commits/5da995e16372f10e2f6d62a605ba5bac62b39f04

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/ji4607/stroboskop/commits/5fa1e74567ea789a854dffb2dd558e22e9b55418

Naloga 6.4.2:
https://bitbucket.org/ji4607/stroboskop/commits/48167fd6131d090535fce233703f946712b4fb73

Naloga 6.4.3:
https://bitbucket.org/ji4607/stroboskop/commits/db820be1655ce4f79f17335c71876069f23a7520

Naloga 6.4.4:
https://bitbucket.org/ji4607/stroboskop/commits/7b03f8cf20b8cbd2c97c148c8a2f1a7f06f537ff